//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    var pokeType:String = "pokeball"
    var name: String    = "No Name"
    var life: String    = "100"
    var hunger:String   = "0"
    var sendMsg: String = ""
    
    var isRunning = false
    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
    
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    
    // MARK: Delegate functions
    // ---------------------
    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        
        // Message from phone comes in this format: ["course":"MADT"]
        //        let messageBody = message["course"] as! String
        self.pokeType = message["poketype"] as! String
        self.name = message["name"] as! String
        self.life = message["life"] as! String
        self.hunger = message["hunger"] as! String
        self.isRunning = NSString(string: message["isrunning"] as! String).boolValue
        
        print("isRunning: \(self.isRunning)")
        messageLabel.setText("\"N\" to enter a name")
        nameLabel.setText(self.name)
        self.pokemonImageView.setImageNamed(self.pokeType)
        self.outputLabel.setText("HP: \(self.life)  Hunger: \(self.hunger)")

        startButtonPressed()
    }
    
    func sendToPhone(message:[String:String]){
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            let message = message
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            print("\nMessage sent to phone")
            // output a debug message to the console
            print("Message sent to watch")
        }
        else {
            print("PHONE: Cannot reach watch")
            print("\nCannot reach phone")
        }
    }
    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // 1. Check if the watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        print(pokeType)
        self.pokemonImageView.setImageNamed(pokeType)
        self.nameLabel.setText(name)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    // MARK: Functions for Pokemon Parenting
    @IBAction func nameButtonPressed() {
        print("name button pressed")
        // 1. Show the built in UI for accepting user input
        let responses = ["Albert", "Antonio", "Emad", "Jenelle", "Pritesh"]
        presentTextInputController(withSuggestions: responses, allowedInputMode: .plain) {
            (results) in
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                //                print(userResponse)
                self.nameLabel.setText(userResponse)
                self.name = userResponse!
            }
        }
    }
    
    @IBAction func startButtonPressed() {
        print("Start button pressed")
        isRunning = true
        let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true) { timer in
            if (self.isRunning){
                print("Hunger: \(self.hunger)\n\nLife: \(self.life)\n")
                self.outputLabel.setText("HP: \(self.life)  Hunger: \(self.hunger)")
                var hung    = Int(self.hunger)
                var lif     = Int(self.life)
                
                if (lif! <= 0) {
                    print("POKEMON DIED!")
                    timer.invalidate()
                }
                if (hung! < 80) {
                    self.nameLabel.setText("\(self.name) is not hungry!")
                }
                else {
                    self.nameLabel.setText("\(self.name) is hungry!")
                    lif! -= 5
                }
                hung! += 10
                self.hunger = String(hung!)
                self.life = String(lif!)

            }
            else {
                timer.invalidate()
                print("Hunger: \(self.hunger)\n\nLife: \(self.life)\n")
                self.outputLabel.setText("HP: \(self.life)  Hunger: \(self.hunger)")
            }
        }
    }
    
    @IBAction func feedButtonPressed() {
        print("Feed button pressed")
        var hung     = Int(self.hunger)
        hung!       -= 22
        var lif     = Int(self.life)
        self.hunger = String(hung!)
        self.life = String(lif!)
        if (hung! >= 90) {
            self.nameLabel.setText("\(self.name) is hungry!")
            lif! -= 5
        }
        else {
            self.nameLabel.setText("\(self.name) is not hungry!")
        }
        self.sendToPhone(message: ["isrunning":String(self.isRunning), "life":self.life, "hunger":self.hunger, "name":self.name, "poketype":self.pokeType])
    }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
        self.isRunning = false
        self.sendToPhone(message: ["isrunning":String(self.isRunning), "life":self.life, "hunger":self.hunger, "name":self.name, "poketype":self.pokeType])
        
    }
    
}
