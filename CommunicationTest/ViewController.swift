//
//  ViewController.swift
//  CommunicationTest
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate  {
    
    var life:String     = "100"
    var hunger:String   = "0"
    var name:String     = "No Name"
    var pokeType:String = "pikachu"
    var isRunning = true
    
    // MARK: Outlets
    @IBOutlet weak var outputLabel: UITextView!
    
    // MARK: Required WCSessionDelegate variables
    // ------------------------------------------
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        //@TODO
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        //@TODO
    }
    
    // MARK: Receive messages from Watch
    // -----------------------------------
    // 3. This function is called when Phone receives message from Watch
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Watch")
        // Message from phone comes in this format: ["course":"MADT"]
        self.pokeType = message["poketype"] as! String
        self.name = message["name"] as! String
        self.life = message["life"] as! String
        self.hunger = message["hunger"] as! String
        self.isRunning = NSString(string: message["isrunning"] as! String).boolValue
    }
    
    // MARK: Default ViewController functions
    // -----------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        // 1. Check if phone supports WCSessions
        print("view loaded")
        if WCSession.isSupported() {
            outputLabel.insertText("\nPhone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            outputLabel.insertText("\nSession activated")
        }
        else {
            print("Phone does not support WCSession")
            outputLabel.insertText("\nPhone does not support WCSession")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    // -------------------
    @IBAction func sendMessageButtonPressed(_ sender: Any) {
        self.isRunning = true
        // 2. When person presses button, send message to watch
        outputLabel.insertText("\nTrying to send message to watch")
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            let message = ["isrunning":String(self.isRunning), "life":self.life, "hunger":self.hunger, "name":self.name, "poketype":self.pokeType]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nMessage sent to watch")
            // output a debug message to the console
            outputLabel.insertText("\nResuming...\n")
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
    }
    
    func sendToWatch(message:[String:String]){
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            let message = ["isrunning":String(self.isRunning), "life":self.life, "hunger":self.hunger, "name":self.name, "poketype":self.pokeType]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nMessage sent to watch")
            // output a debug message to the console
            print("Message sent to watch")
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
    }
    
    // MARK: Choose a Pokemon actions
    @IBAction func pokemonButtonPressed(_ sender: Any) {
        print("You pressed the pokemon button")
        self.pokeType = "pikachu"
        self.sendToWatch(message: ["isrunning":String(self.isRunning), "life":self.life, "hunger":self.hunger, "name":self.name, "poketype":self.pokeType])
    }
    @IBAction func caterpieButtonPressed(_ sender: Any) {
        print("You pressed the caterpie button")
        self.pokeType = "caterpie"
        self.sendToWatch(message: ["isrunning":String(self.isRunning), "life":self.life, "hunger":self.hunger, "name":self.name, "poketype":self.pokeType])
    }
}

